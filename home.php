<?php get_header(); ?>

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Page Heading
                    <small>Secondary Text</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Projects Row -->
        <?php 
        $post_num = 0;
        if ( have_posts() ) : while ( have_posts() ) : the_post();
            if($post_num == 0) {
              echo '<div class="row">';
            }

            $post_num++;
            get_template_part( 'grid-content', get_post_format() );

            if($post_num == 2) {
              echo '</div>';
              $post_num = 0;
            }
        endwhile; endif;

        if ($post_num != 0) {
          echo '</div>';
        }
        ?>
        <!-- /.row -->
<?php get_footer(); ?>
